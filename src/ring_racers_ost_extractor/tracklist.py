from __future__ import annotations

from dataclasses import dataclass
import logging
from typing import Generator, Iterable

from ring_racers_ost_extractor.cups import Cup, Cups
from ring_racers_ost_extractor.levels import Level, load_all_levels
from ring_racers_ost_extractor.music import MusicDef, load_music_defs
from ring_racers_ost_extractor.session import ExtractionSession


_LOGGER = logging.getLogger(__name__)


@dataclass
class Tracklist:
    tracklist: list[MusicDef]

    @staticmethod
    def load(session: ExtractionSession) -> Tracklist:
        # Replicate Ring Racer's internal logic, see https://git.srb2.org/KartKrew/RingRacers/-/blob/master/src/s_sound.c#L1314
        music_defs_by_lump = {music_def.lump: music_def for music_def in load_music_defs(session)}
        _LOGGER.debug("Loaded music defs: %s", music_defs_by_lump.keys())

        witnessed_lumps = set()
        level_music = [music_defs_by_lump[lump] for lump in _load_level_music_keys(session, witnessed_lumps)]

        important_music = [
            music_def for lump, music_def in music_defs_by_lump.items() if lump not in witnessed_lumps and music_def.important
        ]

        other_music = [
            music_def for lump, music_def in music_defs_by_lump.items() if lump not in witnessed_lumps and not music_def.important
        ]

        return Tracklist([
            *important_music,
            *level_music,
            *other_music,
        ])

def _load_level_music_keys(session: ExtractionSession, witnessed_lumps: set[str]) -> Generator[str, None, None]:
    levels_by_key = {level.key: level for level in load_all_levels(session).levels}
    witnessed_levels = set()

    yield from _get_tutorial_levels_music_keys(levels_by_key.values(), witnessed_levels, witnessed_lumps)
    yield from _get_cups_levels_music_keys(session, levels_by_key, witnessed_levels, witnessed_lumps)
    for key, level in levels_by_key.items():
        if key not in witnessed_levels:
            yield from _get_level_music_keys(level, witnessed_levels, witnessed_lumps)
    
def _get_tutorial_levels_music_keys(levels: Iterable[Level], witnessed_levels: set[str], witnessed_lumps: set[str]) -> Generator[str, None, None]:
    for level in levels:
        if level.type == 'Tutorial':
            yield from _get_level_music_keys(level, witnessed_levels, witnessed_lumps)

def _get_cups_levels_music_keys(session: ExtractionSession, levels_by_key: dict[str, Level], witnessed_levels: set[str], witnessed_lumps: set[str]) -> Generator[str, None, None]:
    cups = Cups.load(session)
    for cup in cups.cups:
        yield from _get_cup_levels_music_keys(cup, levels_by_key, witnessed_levels, witnessed_lumps)

def _get_cup_levels_music_keys(cup: Cup, levels_by_key: dict[str, Level], witnessed_levels: set[str], witnessed_lumps: set[str]) -> Generator[str, None, None]:
    for level_key in cup.levels:
        yield from _get_level_music_keys(levels_by_key[level_key], witnessed_levels, witnessed_lumps)
    for level_key in cup.bonus_levels:
        yield from _get_level_music_keys(levels_by_key[level_key], witnessed_levels, witnessed_lumps)
    yield from _get_level_music_keys(levels_by_key[cup.special_stage], witnessed_levels, witnessed_lumps)

def _get_level_music_keys(level: Level, witnessed_levels: set[str], witnessed_lumps: set[str]) -> Generator[str, None, None]:
    if level.key not in witnessed_levels:
        yield from _get_new_lumps(level.position_music, witnessed_lumps)
        yield from _get_new_lumps(level.music, witnessed_lumps)
        yield from _get_new_lumps(level.associated_music, witnessed_lumps)
        yield from _get_new_lumps(level.encore_music, witnessed_lumps)
        witnessed_levels.add(level.key)

def _get_new_lumps(candidates: Iterable[str], witnessed_lumps: set[str]) -> Generator[str, None, None]:
    for candidate in candidates:
        if candidate not in witnessed_lumps:
            yield candidate
            witnessed_lumps.add(candidate)