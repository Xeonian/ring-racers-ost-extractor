from __future__ import annotations

from dataclasses import dataclass
from io import TextIOWrapper
from typing import ClassVar

from ring_racers_ost_extractor.session import ExtractionSession
from ring_racers_ost_extractor.soc import SOC, SOCBlock


@dataclass
class Cups:
    cups: list[Cup]

    @staticmethod
    def load(session: ExtractionSession) -> Cups:
         with TextIOWrapper(session.maps.open('soc/HEADERS_CUPS.soc')) as cup_soc_file:
             cup_soc = SOC.parse(cup_soc_file)
             return Cups([Cup.from_block(block) 
                          for block in cup_soc.blocks 
                          if block.header.type == Cup.soc_type])
                 


@dataclass
class Cup:
    soc_type: ClassVar[str] = 'Cup'

    levels: list[str]
    bonus_levels: list[str]
    special_stage: str

    @classmethod
    def from_block(cls, block: SOCBlock) -> Cup:
        if block.header.type != cls.soc_type:
            raise ValueError
        
        return cls(
            levels=block.properties['LevelList'].split(','),
            bonus_levels=block.properties['BonusGame'].split(','),
            special_stage=block.properties['SpecialStage']
        )