from __future__ import annotations

from contextlib import contextmanager
from dataclasses import dataclass
from pathlib import Path
from typing import Generator
from zipfile import ZipFile


@dataclass
class ExtractionSession:

    maps: ZipFile
    music: ZipFile
    alt_music: ZipFile

    @staticmethod
    @contextmanager
    def open(data_dir: Path) -> Generator[ExtractionSession, None, None]:
        with ZipFile(data_dir / 'maps.pk3') as maps, \
            ZipFile(data_dir / 'music.pk3') as music, \
            ZipFile(data_dir / 'altmusic.pk3') as alt_music:
            yield ExtractionSession(maps=maps, music=music, alt_music=alt_music)
        
