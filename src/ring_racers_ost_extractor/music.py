from __future__ import annotations

from dataclasses import dataclass
from enum import Enum, auto
from io import TextIOWrapper
from pathlib import Path
from typing import Generator
from zipfile import ZipFile

from ring_racers_ost_extractor.soc import SOC, SOCBlock
from .session import ExtractionSession


class Track(Enum):
    A = auto()
    B = auto()


@dataclass
class MusicDef:
    lump: str
    title: str
    track: Track | None = None
    author: str | None = None
    source: str | None = None
    original_composers: str | None = None
    important: bool = False

    @staticmethod
    def from_block(block: SOCBlock) -> Generator[MusicDef, None, None]:
        # TODO: Need to strip leading backslash if necessary
        lumps = block.header.slot.split(',')
        if len(lumps) == 1:
            yield MusicDef._parse_properties(lumps[0], block.properties)
        else:
            for lump, track in zip(lumps, Track):
                yield MusicDef._parse_properties(lump, block.properties, track)

    @staticmethod
    def _parse_properties(lump: str, properties: dict[str, str], track: Track | None = None) -> MusicDef:
        if lump.startswith('\\'):  # Appears to be ignored by the engine?
            lump = lump[1:]

        return MusicDef(lump, properties.get('Title'), track, properties.get('Author'), 
                        properties.get('Source'), properties.get('OriginalComposers'), 
                        properties.get('Important', 'False') == 'True')



def load_music_defs(session: ExtractionSession) -> list[MusicDef]:
    return [
        *_music_defs_from_archive(session.music),
        *_music_defs_from_archive(session.alt_music)
    ]

def _music_defs_from_archive(archive: ZipFile) -> Generator[MusicDef, None, None]:
    for name in archive.namelist():
        path = Path(name)
        if path.name.startswith('MUSICDEF'):
            with TextIOWrapper(archive.open(name)) as music_def_file:
                soc = SOC.parse(music_def_file)
            for block in soc.blocks:
                yield from MusicDef.from_block(block)