from dataclasses import dataclass
import logging
from pathlib import Path
import re
import shutil
from typing import Generator
from zipfile import ZipFile, ZipInfo

from ring_racers_ost_extractor.session import ExtractionSession
from ring_racers_ost_extractor.tracklist import Tracklist


_LOGGER = logging.getLogger(__name__)


_ASSET_PATTERN = re.compile(r"O_(\w+)(?:\..+)?")


def extract(src: Path, dest: Path):
    with ExtractionSession.open(src) as session:
        assets = _flatten_assets(session)
        _LOGGER.debug("Got assets: %s", assets.keys())
        _LOGGER.info("Total tracks: %s", len(assets))

        for index, track in enumerate(Tracklist.load(session).tracklist):
            track_no = index + 1
            clean_title = track.title.replace('/', '-')
            if track.track:
                dest_file_path = dest / f'{track_no:03} {clean_title}, Track {track.track.name}.ogg'  # TODO: Not all oggs!
            else:
                dest_file_path = dest / f'{track_no:03} {clean_title}.ogg'  # TODO: Not all oggs!
            asset = assets[track.lump]
            with asset.archive.open(asset.info) as src_file, \
                dest_file_path.open('wb') as dest_file:
                shutil.copyfileobj(src_file, dest_file)


@dataclass
class Asset:
    archive: ZipFile
    info: ZipInfo

def _flatten_assets(session: ExtractionSession):
    return {lump: asset for archive in [session.music, session.alt_music] for lump, asset in _flatten_archive_assets(archive)}

def _flatten_archive_assets(archive: ZipFile) -> Generator[tuple[str, Asset], None, None]:
    for info in archive.infolist():
        asset_path = Path(info.filename)
        asset_match = _ASSET_PATTERN.fullmatch(asset_path.name)
        if asset_match:
            lump = asset_match.group(1)
            yield lump, Asset(archive, info)