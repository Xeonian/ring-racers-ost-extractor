from __future__ import annotations

from dataclasses import dataclass
from io import TextIOWrapper
from typing import ClassVar

from ring_racers_ost_extractor.session import ExtractionSession
from ring_racers_ost_extractor.soc import SOC, SOCBlock


_LEVEL_SOCS = [
    'HEADERS_BATTLE.soc',
    'HEADERS_RACE.dat',
    'HEADERS_SPECIAL',
    'HEADERS_TUTORIAL'
]


def load_all_levels(session: ExtractionSession) -> Levels:
    return Levels([
        level for level_soc_path in _LEVEL_SOCS for level in _load_levels(session, level_soc_path).levels 
    ])

def _load_levels(session: ExtractionSession, level_soc_path: str) -> Levels:
    with TextIOWrapper(session.maps.open(f'soc/{level_soc_path}')) as level_soc_file:
        return Levels.from_soc(SOC.parse(level_soc_file))    



@dataclass
class Levels:
    levels: list[Level]

    @staticmethod
    def from_soc(soc: SOC) -> Levels:
        return Levels([Level.from_block(block) 
                       for block in soc.blocks 
                       if block.header.type == Level.soc_type])


@dataclass
class Level:
    soc_type: ClassVar[str] = 'Level'

    key: str
    type: str
    position_music: list[str]
    music: list[str]
    associated_music: list[str]
    encore_music: list[str]

    @staticmethod
    def from_block(block: SOCBlock) -> Level:
        if block.header.type != Level.soc_type:
            raise ValueError
        
        return Level(
            key=block.header.slot,
            type=block.properties.get('TypeOfLevel', ''),
            position_music=_parse_music_lumps(block.properties.get('PositionMusic')),
            music=_parse_music_lumps(block.properties.get('Music')),
            associated_music=_parse_music_lumps(block.properties.get('AssociatedMusic')),
            encore_music=_parse_music_lumps(block.properties.get('EncoreMusic')),
        )
    
def _parse_music_lumps(property: str | None) -> list[str]:
    return property.split(',') if property is not None else []