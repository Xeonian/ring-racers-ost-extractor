from __future__ import annotations

from dataclasses import dataclass
import logging
import re
from typing import Generator, Iterable

_LOGGER = logging.getLogger(__name__)


_COMMENT_PATTERN = re.compile(r"([^#]*)(?:(#.*))?")
_HEADER_PATTERN = re.compile(r"(\S+)(?: (\S+))?")
_PROPERTY_PATTERN = re.compile(r"(\S+) = (.*)")


@dataclass
class SOC:
    blocks: list[SOCBlock]

    @staticmethod 
    def parse(file: Iterable[str]) -> SOC:
        soc = SOC([
            SOCBlock.parse_block(raw_block) for raw_block in _raw_blocks(_line_contents(file))
        ])
        # _LOGGER.debug("Parsed SOC: %s", soc)
        return soc

@dataclass
class SOCBlock:
    header: SOCHeader
    properties: dict[str, str]

    @staticmethod
    def parse_block(raw_block: list[str]) -> SOCBlock:
        header = SOCHeader.parse_header(raw_block[0])

        properties = {}
        for raw_property in raw_block[1:]:
            property_match = _PROPERTY_PATTERN.fullmatch(raw_property)
            properties[property_match.group(1)] = property_match.group(2)

        block = SOCBlock(header, properties)
        # _LOGGER.debug("Parsed block: %s", block)
        return block


@dataclass
class SOCHeader:
    type: str
    slot: str | None

    @staticmethod
    def parse_header(line: str) -> SOCHeader:
        header_match = _HEADER_PATTERN.fullmatch(line)
        assert header_match, f"Failed to parse header: {line}"
        type, slot = header_match.groups()
        return SOCHeader(type, slot)


def _line_contents(file: Iterable[str]) -> Generator[str, None, None]:
    for line in file:
        comment_match = _COMMENT_PATTERN.fullmatch(line.strip())
        assert comment_match, f"Failed to extract line contents: {line}"
        line_content, comment = comment_match.groups()
        if line_content or not comment:
            yield line_content

def _raw_blocks(raw_lines: Iterable[str]) -> Generator[list[str], None, None]:
    block = []
    for line in raw_lines:
        if line:
            block.append(line)
        elif block:
            # _LOGGER.debug("Yielding raw SOC block: %s", block)
            yield block
            block = []

    if block:
        # _LOGGER.debug("Yielding raw SOC block: %s", block)
        yield block
